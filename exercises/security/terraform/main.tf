terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

data "digitalocean_ssh_key" "terraform" {
  for_each = toset(var.ssh_key_names)
  name = each.value
}

data "digitalocean_vpc" "web" {
  name = "web-network"

  depends_on = [digitalocean_vpc.web]
}

# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = var.do_token
}

output "droplets" {
  value = digitalocean_droplet.web
}

output "bastion" {
  value = digitalocean_droplet.bastion
}

resource "digitalocean_vpc" "web" {
  name   = "web-network"
  region = var.droplet_region
  ip_range = "10.10.10.0/24"
}


# Create a web server
resource "digitalocean_droplet" "web" {
  count = var.droplet_count
  image = var.droplet_image
  name = format("${var.droplet_name}-%02.f", count.index + 1)
  region = var.droplet_region
  size = var.droplet_size
  ssh_keys = values(data.digitalocean_ssh_key.terraform)[*].id
  vpc_uuid = digitalocean_vpc.web.id
}

# Create a web server
resource "digitalocean_droplet" "bastion" {
  image = var.droplet_image
  name = "${var.droplet_name}-bastion"
  region = var.droplet_region
  size = var.droplet_size
  ssh_keys = values(data.digitalocean_ssh_key.terraform)[*].id
  vpc_uuid = digitalocean_vpc.web.id
}

resource "digitalocean_firewall" "bastion" {
  name = "bastion"

  droplet_ids = [digitalocean_droplet.bastion.id]

  outbound_rule {
    protocol = "tcp"
    port_range = "22"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "icmp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol = "tcp"
    port_range = "22"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol         = "icmp"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }
}

resource "digitalocean_firewall" "web" {
  name = "webservers"

  droplet_ids = digitalocean_droplet.web.*.id

  outbound_rule {
    protocol = "tcp"
    port_range = "80"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol = "tcp"
    port_range = "443"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "udp"
    port_range            = "53"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "icmp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_droplet_ids = [digitalocean_droplet.bastion.id]
  }

  inbound_rule {
    protocol         = "icmp"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "icmp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol = "tcp"
    port_range = "5000"
    source_load_balancer_uids = [digitalocean_loadbalancer.public.id]
  }
}

resource "digitalocean_loadbalancer" "public" {
  name   = var.loadbalancer_name
  region = var.droplet_region

  forwarding_rule {
    entry_port     = var.entry_port
    entry_protocol = var.entry_protocol

    target_port     = var.target_port
    target_protocol = var.entry_protocol
  }

  healthcheck {
    port     = var.target_port
    protocol = var.entry_protocol
    path     = var.healthcheck_path
  }

  droplet_ids = digitalocean_droplet.web.*.id
  vpc_uuid = digitalocean_vpc.web.id
}

resource "digitalocean_domain" "www" {
  name = var.domain_name
  ip_address = digitalocean_loadbalancer.public.ip
}
