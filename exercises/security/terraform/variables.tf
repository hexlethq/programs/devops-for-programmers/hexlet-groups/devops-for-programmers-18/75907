variable "do_token" {
  type = string
  sensitive = true
}

variable "domain_name" {
  type = string
  default = "dzencot.xyz"
}

variable "ssh_key_names" {
  type = list(string)
  default = ["ivan-key", "work"]
}

variable "loadbalancer_name" {
  type = string
  default = "balancer-security-homework"
}

variable "droplet_count" {
  type = number
  default = 2
}

variable "droplet_name" {
  type    = string
  default = "droplet-security-homework"
}

variable "droplet_image" {
  type = string
  default = "ubuntu-20-10-x64"
}

variable "droplet_region" {
  type = string
  default = "ams3"
}

variable "droplet_size" {
  type = string
  default = "s-1vcpu-1gb"
}

variable "entry_port" {
  type = number
  default = 80
}

variable "entry_protocol" {
  type = string
  default = "http"
}

variable "target_port" {
  type = number
  default = 5000
}

variable "healthcheck_path" {
  type = string
  default = "/"
}
